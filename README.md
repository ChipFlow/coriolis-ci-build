# Coriolis CI Build

This repository has a CI pipeline which builds Coriolus and publishes it as a 
job artifact. This is used to lock to a verison of Coriolis before we get its
python packaging completed.

## Notable builds

If you want to add a new build, you will probably want to ensure that GitLab
keeps the job artifacts, which you can set in the UI.

* Coriolis version [db56681025e022eafe1262b1c91eea6b0f0ae803](https://gitlab.lip6.fr/vlsi-eda/coriolis/-/tree/db56681025e022eafe1262b1c91eea6b0f0ae803)
  * Fixing stacked via for PragmatIC
  * Artifacts: https://gitlab.com/api/v4/projects/ChipFlow%2Fcoriolis-ci-build/jobs/3792055503/artifacts
* Coriolis version [77afb7cba4e2e39103d0238fb52e768dadc0d1e8](https://gitlab.lip6.fr/vlsi-eda/coriolis/-/tree/77afb7cba4e2e39103d0238fb52e768dadc0d1e8)
  * Artifacts: https://gitlab.com/api/v4/projects/ChipFlow%2Fcoriolis-ci-build/jobs/3742133839/artifacts
* Coriolis version [908231c4c4a9c1314bf9336fc523333b5f3ef3ca](https://gitlab.lip6.fr/vlsi-eda/coriolis/-/tree/908231c4c4a9c1314bf9336fc523333b5f3ef3ca)
  * Used for prototype Sky130 builder + MPW5.
  * Artifacts: https://gitlab.com/api/v4/projects/ChipFlow%2Fcoriolis-ci-build/jobs/3268298141/artifacts

